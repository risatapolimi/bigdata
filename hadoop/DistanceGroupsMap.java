import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class DistanceGroupsMap extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, BooleanWritable>{

	@Override
	public void map(LongWritable key, Text value, OutputCollector<IntWritable, BooleanWritable> output, Reporter reporter) throws IOException {
		String[] fields = value.toString().split(",");
		int arrDelay;
		int depDelay;
		int distance;

		if(!fields[14].equals("NA") && !fields[15].equals("NA") && !fields[18].equals("NA") && !fields[0].equals("Year")) { //JUMP first line
			arrDelay = Integer.parseInt(fields[14]);
			depDelay = Integer.parseInt(fields[15]);
			distance = Integer.parseInt(fields[18]);
			if(distance<2600 && depDelay>0) { //Check conditions requested
				if(arrDelay<=(0.5*depDelay)) {
					output.collect(new IntWritable(getDistanceGroup(distance)), new BooleanWritable(true));
				}else {
					output.collect(new IntWritable(getDistanceGroup(distance)), new BooleanWritable(false));
				}
			}
			
		}
	}
	
	private int getDistanceGroup(int distance) {
		return (distance/200)+1;	
	}
	
}
