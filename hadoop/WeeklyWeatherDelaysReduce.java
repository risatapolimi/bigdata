import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class WeeklyWeatherDelaysReduce extends MapReduceBase implements Reducer<YearWeekKey, DelayDataValue, Text, DoubleWritable>{

	private DoubleWritable result = new DoubleWritable();
	private Text keyText = new Text();
	
	@Override
	public void reduce(YearWeekKey key, Iterator<DelayDataValue> values, OutputCollector<Text, DoubleWritable> output, Reporter reporter) throws IOException {
		//Counters
		int totalDelay = 0;
		int weatherDelay = 0;
		DelayDataValue value;
		while(values.hasNext()) {
			value = values.next();
			totalDelay += value.getTotal_delay().get();
			weatherDelay += value.getWeather_delay().get(); 
		}
		//Compute percentage
		result.set((weatherDelay*100.0f)/totalDelay);
		keyText.set(key.toString());
		output.collect(keyText, result);
		
	}

}
