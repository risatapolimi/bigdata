import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class DistanceGroupsReduce extends MapReduceBase implements Reducer<IntWritable, BooleanWritable, Text, DoubleWritable>{

	private DoubleWritable result = new DoubleWritable();
	@Override
	public void reduce(IntWritable key, Iterator<BooleanWritable> values, OutputCollector<Text, DoubleWritable> output, Reporter reporter) throws IOException {
		//Counters
		int totalFlights = 0;
		int halveDelayFlights = 0;
		boolean halved;
		
		while(values.hasNext()) {
			halved = values.next().get();
			totalFlights++;
			if(halved) {
				halveDelayFlights++;
			}
		}
		//Compute percentage
		result.set((halveDelayFlights*100.0f)/totalFlights);
		output.collect(new Text("Group[" + key + "]: "), result);
		
		
	}

}
