

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;


public class CanceledFlightsReduce extends MapReduceBase implements Reducer<Text, Text, Text, DoubleWritable> {
    
	private DoubleWritable result = new DoubleWritable();

	@Override
	public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, DoubleWritable> output, Reporter reporter) throws IOException {
		//Counters
		int totalFlight = 0;
	    int canceledFlight = 0;
	    String stringValue;
	    while(values.hasNext()) {
	    	stringValue = values.next().toString();
	    	if(!stringValue.equals("NA")) { //Consider only available data
	    		totalFlight ++;
		    	if(stringValue.equals("yes")){
		    		canceledFlight++;
		    	}
	    	}
	    }
	    //Perform percentage
	    result.set((canceledFlight*100.0f)/totalFlight);
	    output.collect(key, result);
	     
	}

}