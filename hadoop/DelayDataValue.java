import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
/*
 * Value class used for "WeeklyWeatherDelays" to store information regarding total delay and fraction of delay due to weather
 */
public class DelayDataValue implements  WritableComparable<DelayDataValue> {

	private IntWritable total_delay;
	private IntWritable weather_delay;
	
	public DelayDataValue(IntWritable total_delay, IntWritable weather_delay) {
		super();
		this.total_delay = total_delay;
		this.weather_delay = weather_delay;
	}
	
	public DelayDataValue() {
		super();
		this.total_delay = new IntWritable();
		this.weather_delay = new IntWritable();
	}


	@Override
	public void write(DataOutput out) throws IOException {
		this.total_delay.write(out);
		this.weather_delay.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.total_delay.readFields(in);
		this.weather_delay.readFields(in);
	}

	@Override
	public int compareTo(DelayDataValue tp) {
		int cmp = total_delay.compareTo(tp.total_delay);
		 
        if (cmp != 0) {
            return cmp;
        }
 
        return weather_delay.compareTo(tp.weather_delay);
	}

	public IntWritable getTotal_delay() {
		return total_delay;
	}

	public void setTotal_delay(IntWritable total_delay) {
		this.total_delay = total_delay;
	}

	public IntWritable getWeather_delay() {
		return weather_delay;
	}

	public void setWeather_delay(IntWritable weather_delay) {
		this.weather_delay = weather_delay;
	}

	
}
