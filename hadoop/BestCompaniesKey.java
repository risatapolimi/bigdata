import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
/*
 * Key class containing origin and destination of an air travel
 */
public class BestCompaniesKey implements WritableComparable<BestCompaniesKey> {

	private Text origin;
	private Text destination;
	
	
	public BestCompaniesKey(Text origin, Text destination) {
		super();
		this.origin = origin;
		this.destination = destination;
	}
	
	public BestCompaniesKey() {
		super();
		this.origin = new Text();
		this.destination = new Text();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.origin.write(out);
		this.destination.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.origin.readFields(in);
		this.destination.readFields(in);
	}

	@Override
	public int compareTo(BestCompaniesKey o) {
		int cmp = origin.compareTo(o.origin);
        if (cmp != 0) {
            return cmp;
        }
        return destination.compareTo(o.destination);
	}

	public Text getOrigin() {
		return origin;
	}

	public void setOrigin(Text origin) {
		this.origin = origin;
	}

	public Text getDestination() {
		return destination;
	}

	public void setDestination(Text destination) {
		this.destination = destination;
	}
	
	

}
