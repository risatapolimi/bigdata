
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
 

/*
 * Old example, see other
 */
public class HadoopProject {
 
  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, BooleanWritable>{
 
    private final static BooleanWritable myTrue = new BooleanWritable(true);
    private final static BooleanWritable myFalse = new BooleanWritable(false);
    
    private Text day = new Text();
 
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      //StringTokenizer itr = new StringTokenizer(value.toString(),",");
      String[] fields = value.toString().split(",");
      
      day.set(fields[0]+fields[1]+fields[2]);
      
      if(fields[21].equals("1")){
        context.write(day,myTrue);
      }else{
        context.write(day,myFalse);
      }
    }
  }
 
  public static class IntSumReducer
       extends Reducer<Text,BooleanWritable,Text,DoubleWritable> {
    private DoubleWritable result = new DoubleWritable();
 
    public void reduce(Text key, Iterable<BooleanWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int totalFlight = 0;
      int canceledFlight = 0;
      for (BooleanWritable val : values) {
        totalFlight ++;
        if(val.get()==true){
          canceledFlight++;
        }
      }
      result.set((canceledFlight*100.0f)/totalFlight);
      context.write(key, result);
    }
  }
 
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "percentage canceled flights per day");
    job.setJarByClass(HadoopProject.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(BooleanWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
