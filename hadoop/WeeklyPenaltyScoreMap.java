import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class WeeklyPenaltyScoreMap extends MapReduceBase implements Mapper<LongWritable, Text, PenaltyScoreKey, DoubleWritable>{

	@Override
	public void map(LongWritable key, Text value, OutputCollector<PenaltyScoreKey, DoubleWritable> output, Reporter reporter) throws IOException {
		String[] fields = value.toString().split(",");
		
		int year;
		int week_of_year;
		int delay;
		String airport;
		
	
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calUs = Calendar.getInstance(Locale.US); //US calendar
		if(!fields[0].equals("Year")) { //jump first line
			year = Integer.parseInt(fields[0]); 
			try {
				Date  date = sdf.parse(fields[2]+"/"+fields[1]+"/"+fields[0]);
				calUs.setTime(date);
			    //Retrieve week number in year
				week_of_year = calUs.get( Calendar.WEEK_OF_YEAR ); 
			    
			    airport = fields[17]; //destination --> incoming 0.5 score
			    if(!airport.equals("NA") && !fields[14].equals("NA")){ //Check fields
			    	delay = Integer.parseInt(fields[14]);
			    	if(delay>15) {
			    		output.collect(new PenaltyScoreKey(new IntWritable(year), new IntWritable(week_of_year), new Text(airport)), new DoubleWritable(0.5));
			    	}
			    }
			    
			    airport = fields[16]; //origin -->outgoing 1 score
			    if(!airport.equals("NA") && !fields[15].equals("NA")){ //Check fields
			    	delay = Integer.parseInt(fields[15]);
			    	if(delay>15) {
			    		output.collect(new PenaltyScoreKey(new IntWritable(year), new IntWritable(week_of_year), new Text(airport)), new DoubleWritable(1));
			    	}
			    }
			    
			}catch (ParseException e) {
				//  Nothing to do 
				e.printStackTrace();
			}
		}
			
	}
}
