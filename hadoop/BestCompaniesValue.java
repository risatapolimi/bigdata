import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
/*
 * Value class containing sum of all delays, air time and carrier about an air travel
 */
public class BestCompaniesValue implements  WritableComparable<BestCompaniesValue>{

	private IntWritable sumDelay;
	private IntWritable airTime;
	private Text carrier;
	
	public BestCompaniesValue(IntWritable sumDelay, IntWritable airTime, Text carrier) {
		super();
		this.sumDelay = sumDelay;
		this.airTime = airTime;
		this.carrier = carrier;
	}

	public BestCompaniesValue() {
		super();
		this.sumDelay = new IntWritable();
		this.airTime = new IntWritable();
		this.carrier = new Text();
	}
	
	
	@Override
	public void write(DataOutput out) throws IOException {
		this.sumDelay.write(out);
		this.airTime.write(out);
		this.carrier.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.sumDelay.readFields(in);
		this.airTime.readFields(in);
		this.carrier.readFields(in);
	}

	@Override
	public int compareTo(BestCompaniesValue other) {
		int cmp = sumDelay.compareTo(other.sumDelay);
        if (cmp != 0) {
            return cmp;
        }
        cmp = airTime.compareTo(other.airTime);
        if (cmp != 0) {
            return cmp;
        }
        
        return carrier.compareTo(other.carrier);
	}

	public IntWritable getSumDelay() {
		return sumDelay;
	}

	public void setSumDelay(IntWritable sumDelay) {
		this.sumDelay = sumDelay;
	}

	public IntWritable getAirTime() {
		return airTime;
	}

	public void setAirTime(IntWritable airTime) {
		this.airTime = airTime;
	}

	public Text getCarrier() {
		return carrier;
	}

	public void setCarrier(Text carrier) {
		this.carrier = carrier;
	}
	
	

}
