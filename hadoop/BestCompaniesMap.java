import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class BestCompaniesMap extends MapReduceBase implements Mapper<LongWritable, Text, BestCompaniesKey, BestCompaniesValue>{

	private BestCompaniesKey bestCompaniesKey;
	private BestCompaniesValue BestCompaniesValue;
	
	@Override
	public void map(LongWritable key, Text value, OutputCollector<BestCompaniesKey, BestCompaniesValue> output, Reporter reporter) throws IOException {
		String[] fields = value.toString().split(",");
		
		//Key
		String origin;
		String destination;
		//Value
		int arrDelay = 0;
		int depDelay = 0;
		int taxiIn = 0;
		int taxiOut = 0;
		int sumDelay;
		int airTime = 0;
		String carrier;
		
		
		if(!fields[0].equals("Year")) { //jump first line
			if(!fields[8].equals("NA") && !fields[13].equals("NA") && !fields[14].equals("NA") && !fields[15].equals("NA") &&  //Check available fields
					!fields[16].equals("NA") && !fields[17].equals("NA") &&	!fields[19].equals("NA") && !fields[20].equals("NA")) {
				//Key
				origin = fields[16];
				destination = fields[17];
				bestCompaniesKey = new BestCompaniesKey(new Text(origin), new Text(destination));
				
				//Value
				arrDelay = Integer.parseInt(fields[14]);
				depDelay = Integer.parseInt(fields[15]);
				taxiIn = Integer.parseInt(fields[19]);
				taxiOut = Integer.parseInt(fields[20]);
				
				sumDelay = arrDelay + depDelay + taxiIn + taxiOut;
				airTime = Integer.parseInt(fields[13]);
				carrier = fields[8];
				if(sumDelay >= 0 && airTime >= 0) { 					
					BestCompaniesValue = new BestCompaniesValue(new IntWritable(sumDelay), new IntWritable(airTime), new Text(carrier));
					
					output.collect(bestCompaniesKey, BestCompaniesValue);
				}
			}
		}	
	}
}
