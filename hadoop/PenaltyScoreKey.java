import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
/*
 * Key class used to store information regarding the #week of the year and the airport
 */
public class PenaltyScoreKey implements WritableComparable<PenaltyScoreKey>{

	private IntWritable year;
	private IntWritable week;
	private Text airport;
	
	public PenaltyScoreKey(IntWritable year, IntWritable week, Text airport) {
		super();
		this.year = year;
		this.week = week;
		this.airport = airport;
	}
	
	public PenaltyScoreKey() {
		super();
		this.year = new IntWritable();
		this.week = new IntWritable();
		this.airport = new Text();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.year.write(out);
		this.week.write(out);
		this.airport.write(out);
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.year.readFields(in);
		this.week.readFields(in);
		this.airport.readFields(in);
	}

	@Override
	public int compareTo(PenaltyScoreKey other) {
		int cmp = year.compareTo(other.year);
        if (cmp != 0) {
            return cmp;
        }
        cmp = week.compareTo(other.week);
        if (cmp != 0) {
            return cmp;
        }
        
        return airport.compareTo(other.airport);
	}

	public IntWritable getYear() {
		return year;
	}

	public void setYear(IntWritable year) {
		this.year = year;
	}

	public IntWritable getWeek() {
		return week;
	}

	public void setWeek(IntWritable week) {
		this.week = week;
	}

	public Text getAirport() {
		return airport;
	}

	public void setAirport(Text airport) {
		this.airport = airport;
	}
	
	@Override
	public String toString() {
		return "Airport: "+ airport + " [ " + year + " ] #" + week;
	}

}
