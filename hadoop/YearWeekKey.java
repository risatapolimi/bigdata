import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
/*
 * Key class used for "WeeklyWeatherDelays" to store information regarding the week of the year
 */
public class YearWeekKey implements  WritableComparable<YearWeekKey> {

	private IntWritable year;
	private IntWritable week_of_year;
	
	public YearWeekKey(IntWritable year, IntWritable week_of_year) {
		super();
		this.year = year;
		this.week_of_year = week_of_year;
	}
	

	public YearWeekKey() {
		super();
		this.year = new IntWritable();
		this.week_of_year = new IntWritable();
	}



	@Override
	public void write(DataOutput out) throws IOException {
		this.year.write(out);
		this.week_of_year.write(out);

	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.year.readFields(in);
		this.week_of_year.readFields(in);
	}

	@Override
	public int compareTo(YearWeekKey tp) {
		int cmp = year.compareTo(tp.year);
        if (cmp != 0) {
            return cmp;
        }
        return week_of_year.compareTo(tp.week_of_year);
	}

	public IntWritable getYear() {
		return year;
	}

	public void setYear(IntWritable year) {
		this.year = year;
	}

	public IntWritable getWeek_of_year() {
		return week_of_year;
	}

	public void setWeek_of_year(IntWritable week_of_year) {
		this.week_of_year = week_of_year;
	}

	@Override
	public String toString() {
		return "[ " + year + " ] #" + week_of_year + ": ";
	}
	
	
}
