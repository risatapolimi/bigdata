/*
 * Class used for "BestCompanies" reduce
 */
public class CompanyAirTime implements Comparable<CompanyAirTime> {

	String carrier;
	Double airTime;

	public CompanyAirTime(String carrier, Double airTime) {
		super();
		this.carrier = carrier;
		this.airTime = airTime;
	}

	@Override
	public int compareTo(CompanyAirTime o) {
		return airTime.compareTo(o.airTime);
	}

	@Override
	public String toString() {
		return "[" + carrier + ", airTime=" + airTime + "]";
	}

}
