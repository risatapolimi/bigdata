import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class WeeklyPenaltyScoreReduce  extends MapReduceBase implements Reducer<PenaltyScoreKey, DoubleWritable, Text, DoubleWritable>{

	private Text result = new Text();
	
	@Override
	public void reduce(PenaltyScoreKey key, Iterator<DoubleWritable> values, OutputCollector<Text, DoubleWritable> output, Reporter reporter) throws IOException {
	
		//Counters
		double totalScore = 0;
		double partialScore = 0;
		
		while(values.hasNext()) {
			partialScore = values.next().get();
			totalScore +=partialScore;
		}
		
		//Output final score
		result.set(key.toString());
		output.collect(result, new DoubleWritable(totalScore));
		
		
		
	}
	
	
}
