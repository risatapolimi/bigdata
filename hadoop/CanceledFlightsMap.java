


import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class CanceledFlightsMap extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

	private Text date = new Text();
	private Text isCancel = new Text();

	 
	@Override
	public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
		String[] fields = value.toString().split(",");
		if(!fields[0].equals("Year")) {
			//Save date as key
		    date.set(fields[0] + " - "+ fields[1] + " - " + fields[2]);
		      
		    //Check if cancelled and output key value pair
		    if(fields[21].equals("1")){
		    	isCancel.set("yes");
		    	output.collect(date,isCancel);
		    }else if(fields[21].equals("0")){ 
		    	isCancel.set("no");
		    	output.collect(date,isCancel);
		    }else { //NA Values
		    	isCancel.set("NA");
		    	output.collect(date,isCancel);
		    }
		}
		
	}

}