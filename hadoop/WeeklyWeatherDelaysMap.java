import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class WeeklyWeatherDelaysMap extends MapReduceBase implements Mapper<LongWritable, Text, YearWeekKey, DelayDataValue>{

	private YearWeekKey year_week;
	private  DelayDataValue delay;
	
	@Override
	public void map(LongWritable key, Text value, OutputCollector<YearWeekKey, DelayDataValue> output, Reporter reporter) throws IOException {
		String[] fields = value.toString().split(",");
		
		int year;
		int week_of_year;
		
		int total_delay;
		int weather_delay;
		
		
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calUs = Calendar.getInstance(Locale.US);
		if(!fields[0].equals("Year")) { //jump first line
			year = Integer.parseInt(fields[0]); 
			try {
				Date  date = sdf.parse(fields[2]+"/"+fields[1]+"/"+fields[0]);
			    calUs.setTime(date);
			    //Retrieve week of the year
			    week_of_year = calUs.get( Calendar.WEEK_OF_YEAR );
			    
			    //Key
			    this.year_week = new YearWeekKey(new IntWritable(year), new IntWritable(week_of_year));
			    
			    //Value
			    if(!fields[14].equals("NA") && !fields[15].equals("NA") && !fields[25].equals("NA")) {
			    	total_delay = Integer.parseInt(fields[14])+ Integer.parseInt(fields[15]);
			    	weather_delay = Integer.parseInt(fields[25]);
			    	//Check if the total delay is positive, otherwise it is not a delay
			    	if(total_delay > 0) {
			    		this.delay = new DelayDataValue(new IntWritable(total_delay), new IntWritable(weather_delay));
						
				    	output.collect(year_week,delay);
			    	}
				}
			} catch (ParseException e) {
				// Nothing to do
				e.printStackTrace();
			}
		}
		
	
		
	    
	  
	    
	    
		
		
		
	}

}
