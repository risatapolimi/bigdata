import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class BestCompaniesReduce  extends MapReduceBase implements Reducer<BestCompaniesKey, BestCompaniesValue, Text, Text>{

	private Text keyResult; 
	private Text valueResult;
	
	@Override
	public void reduce(BestCompaniesKey key, Iterator<BestCompaniesValue> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
		String airRoute = "["+key.getOrigin()+"-"+key.getDestination()+"]";
		
		Map<String,List<Integer>> carrierToDelayList = new HashMap<>();
		Map<String,List<Integer>> carrierToAirTimeList = new HashMap<>();
		List<String> carrierList = new ArrayList<>();
		String carrier;
		
		BestCompaniesValue value;
		while(values.hasNext()) {
			value = values.next();
			//Carrier value
			carrier = value.getCarrier().toString();
			if(!carrierList.contains(carrier)) {
				carrierList.add(carrier);
			}
			
			
			//fill delay map
			if(carrierToDelayList.get(carrier) == null) {
				carrierToDelayList.put(carrier, new ArrayList<>());
				carrierToDelayList.get(carrier).add(value.getSumDelay().get());
			}else {
				carrierToDelayList.get(carrier).add(value.getSumDelay().get());
			}
			
			//fill airTime map
			if(carrierToAirTimeList.get(carrier) == null) {
				carrierToAirTimeList.put(carrier, new ArrayList<>());
				carrierToAirTimeList.get(carrier).add(value.getAirTime().get());

			}else {
				carrierToAirTimeList.get(carrier).add(value.getAirTime().get());
			}	
		}
		
		//companies and their air time
		List<CompanyAirTime> companyAirTimeList = new ArrayList<>();
		for(String c: carrierList) {
			//Average of all delays
			Double meanDelay = carrierToDelayList.get(c).stream().mapToInt(val -> val).average().orElse(0.0);
			//Best air time value (the minimum)
			int indexBestAirTime = carrierToAirTimeList.get(c).indexOf(Collections.min(carrierToAirTimeList.get(c)));
			int bestAirTime = carrierToAirTimeList.get(c).get(indexBestAirTime);

			double airTime = bestAirTime + meanDelay;
			companyAirTimeList.add(new CompanyAirTime(c, airTime));
		}
		
		//Sort
		companyAirTimeList.sort((p1, p2) -> p1.compareTo(p2));
		String resultString = "";
		int index = 0;
		//Define 3 best carrier for a given origin-destination travel
		while(index<3 && index < companyAirTimeList.size()) {
			String carr = companyAirTimeList.get(index).toString();
			resultString += "#" + (index+1) + ": " + carr + " ";
			index++;
		}
		
		keyResult = new Text(airRoute);
		valueResult = new Text(resultString);
		output.collect(keyResult, valueResult);
	}
}
