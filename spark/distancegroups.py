import datetime

from pyspark import SparkContext
sc =SparkContext()

#torna il gruppo data la distanza
def distanceGroup(distanceMiles):
	return ((distanceMiles/200)+1)

#Apertura file e divisione in campi
flights = sc.textFile("file:///mnt/scambiovbox/input")\
	.flatMap(lambda line: line.split("\n"))\
    .map(lambda line: line.split(",")) \
    .filter(lambda line: line[0] != "Year" and line[15]!="NA" and line[14]!="NA" and line[18]!="NA"and int(line[15]) > 0 and int(line[18])<2600)\
    .map(lambda line: (distanceGroup(int(line[18])), (int(line[15]), int(line[14]) ))) #coppia (distanceGroup, (depDelay, arrDelay ))

print "\nValori FLIGHT \n\n"
#print flights.take(10)

#crea la coppia che indica se il volo ha dimezzato il ritardo
mapFlights = flights.map(lambda x: (x[0],  (1 if(x[1][1] <= 0.5*x[1][0]) else 0))) #coppia (distanceGroup, halved_1_0)

print "\nValori REDUCED \n\n"
#print mapFlights.take(10)

#prepara la coppia per il calcolo della percentuale
sumCount = mapFlights.combineByKey(lambda value: (int(value), int(1)),
                             lambda x, value: (int(x[0]) + int(value), x[1] + 1),
                             lambda x, y: (int(x[0]) + int(y[0]), x[1] + y[1])) #coppia (distanceGroup, (sumHalved, countTotalPerGroup))

mapPercentage = sumCount.map(lambda x: (x[0], (x[1][0]*1.0/x[1][1])*100)) #coppia (group, percentage) 

#print mapPercentage.take(10)

from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

df = sqlContext.createDataFrame(mapPercentage, ['group', 'percentage'])

df.coalesce(1).write.format('com.databricks.spark.csv').options(header='true').save('file:///mnt/scambiovbox/output/distance')

