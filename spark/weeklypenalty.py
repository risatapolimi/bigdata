import datetime

from pyspark import SparkContext
sc =SparkContext()

#Apertura file e divisione in campi
flights = sc.textFile("file:///mnt/scambiovbox/input")\
	.flatMap(lambda line: line.split("\n"))\
    .map(lambda line: line.split(",")) \
    .filter(lambda line: line[17]!="NA" and line[16]!="NA" and line[14]!="NA" and line[15]!="NA" and line[0] != "Year")\
    .map(lambda line: ((line[0],datetime.date(int(line[0]),int(line[1]),int(line[2])).strftime("%V")), (line[16], line[17], int(line[15]) , int(line[14]) ))) #coppia (Year,weekOfYear) , (origin, destination, depDelay, arrDelay ))

print "\nValori FLIGHT \n\n"
#print flights.take(10)

originFlights = flights.map(lambda x: ((x[0][0],x[0][1],x[1][0]), 1 if(x[1][2]>15) else 0 )) #coppia ((year,weekOfYear, Airport), originDelay_1_0)
destFlights = flights.map(lambda x: ((x[0][0],x[0][1],x[1][1]), 0.5 if(x[1][3]>15) else 0 )) #coppia ((year,weekOfYear, Airport), destDelay_1_0)

#print originFlights.take(10)

#unisce i due dataset di origine e destinazione
allFlights = sc.union([originFlights, destFlights])

#print allFlights.take(10)

#somma i punteggi per aeroporto
primaReduce = allFlights.reduceByKey(lambda x,y: x + y) #coppia ((year,weekOfYear, Airport), score)

#flatten the key
mapToFile = primaReduce.map(lambda x: (x[0][0],x[0][1], x[0][2], x[1])) #coppia #coppia (year, weekOfYear, Airport, score)
#print primaReduce.take(10)

from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

df = sqlContext.createDataFrame(mapToFile, ['year', 'week', 'airport', 'score'])


df.coalesce(1).write.format('com.databricks.spark.csv').options(header='true').save('file:///mnt/scambiovbox/output/penalty1')

