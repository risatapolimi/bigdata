import pandas as pd
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource
from bokeh.palettes import Spectral3

output_file('/mnt/scambiovbox/charts/weather.html')

df = pd.read_csv('/mnt/scambiovbox/output/weatherdelay2/part-00000-3201bad8-5bd9-4a05-99c6-a28f7c0ed74a-c000.csv')

sorteddf = df.sort_values(by=['year', 'week'])
df2 = sorteddf.reset_index(drop=True)


df2['period'] = df2[['year', 'week']].astype(str).apply(lambda x: 'w'.join(x), axis=1)


source = ColumnDataSource(df2)

p = figure(x_range=df2.period,plot_width=1200, plot_height=600, tooltips=[('week', '@period'),('percentage', '@percentage')])

#p.line(x='index', y='percentage', line_width=2, source=source, legend='Weather percentage')
#p.step(x='index', y='percentage', line_width=2, source=source, legend='Weather percentage', mode="center")
#p.circle(x='index', y='percentage', source=source, fill_color="white", size=4)
#p.circle(x='period', y='percentage', source= source, size=1, color="navy", alpha=0.9)
p.vbar(x='index', top='percentage' , source=source, width=0.5, color = "#ff1200", bottom=0)
p.xaxis.major_label_orientation = 3.14/4

p.yaxis.axis_label = 'Percentage of weather delayed flights per week'
p.xaxis.axis_label = 'Weeks of years'

show(p)