import pandas as pd
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource
from bokeh.palettes import Spectral3

output_file('/mnt/scambiovbox/charts/distance.html')

df2 = pd.read_csv('/mnt/scambiovbox/output/distance/part-00000-cd550177-7e42-4445-9fa8-a6d5fa319df6-c000.csv')

source = ColumnDataSource(df2)

p = figure(plot_width=1000, plot_height=500, tooltips=[('group', '@group'),('percentage', '@percentage')])

#p.line(x='index', y='percentage', line_width=2, source=source, legend='Weather percentage')
#p.step(x='index', y='percentage', line_width=2, source=source, legend='Weather percentage', mode="center")
#p.circle(x='index', y='percentage', source=source, fill_color="white", size=4)
#p.circle(x='period', y='percentage', source= source, size=1, color="navy", alpha=0.9)
p.vbar(x='group', top='percentage' , source=source, width=0.5, color = "#ff1200", bottom=0)
p.xaxis.major_label_orientation = 3.14/4

p.yaxis.axis_label = 'Percentage of halved delays per distance group'
p.xaxis.axis_label = 'Distance group'

show(p)