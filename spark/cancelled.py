from pyspark import SparkContext
sc =SparkContext()

#Apertura file e divisione in campi
flights = sc.textFile("file:///mnt/scambiovbox/input") \
	.flatMap(lambda line: line.split("\n"))\
    .map(lambda line: line.split(",")) \
    .filter(lambda line: line[21]!="NA" and line[0] != "Year")\
    .map(lambda line: (line[0]+"-" +line[1]+"-"+line[2], line[21])) #coppia (date, cancelledFlag0_1)

print "\nValori FLIGHT KEYS\n\n"
print flights.keys().take(10)

#creazione coppia per il calcolo della percentuale
sumCount = flights.combineByKey(lambda value: (int(value), int(1)),
                             lambda x, value: (int(x[0]) + int(value), x[1] + 1),
                             lambda x, y: (int(x[0]) + int(y[0]), x[1] + y[1])) #coppia (date, (sumCancelled, countTotalPerData))

print sumCount.take(10)

#calcolo della percentuale
mapPercentage = sumCount.map(lambda x: (x[0], (x[1][0]*1.0/x[1][1])*100)) #coppia (date, percentage)

print mapPercentage.take(10)

sortedPercentage = mapPercentage.sortBy(lambda x: x[0])

#esportazione dati in csv
def toCSVLine(data):
  return ','.join(str(d) for d in data)

from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

df = sqlContext.createDataFrame(sortedPercentage, ['date', 'percentage'])

df.coalesce(1).write.format('com.databricks.spark.csv').options(header='true').save('file:///mnt/scambiovbox/output/canceled')

