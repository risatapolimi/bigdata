import datetime

from pyspark import SparkContext
sc =SparkContext()

#Apertura file e divisione in campi
flights = sc.textFile("file:///mnt/scambiovbox/input")\
	.flatMap(lambda line: line.split("\n"))\
    .map(lambda line: line.split(",")) \
    .filter(lambda line: line[25]!="NA" and line[14]!="NA" and line[15]!="NA" and line[0] != "Year")\
    .map(lambda line: ((line[0],datetime.date(int(line[0]),int(line[1]),int(line[2])).strftime("%V")), (int(line[25]), int(line[14]) + int(line[15]) ))) #coppia ((Year,weekOfYear), (WeatherDelay, totalDelay ))

print "\nValori FLIGHT \n\n"
#print flights.take(10)

primaReduce = flights.reduceByKey(lambda x,y: (x[0] + y[0], x[1] + y[1]) ).filter(lambda x: x[1][1] > 0) #coppia ((year,weekOfYear), (TotalWeatherDelay, TotalDelay))

#print primaReduce.take(10)

mapPercentage = primaReduce.map(lambda x: (x[0][0], x[0][1], (x[1][0]*1.0/x[1][1])*100)) #coppia (date, week, percentage)

#print mapPercentage.take(10)

from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

df = sqlContext.createDataFrame(mapPercentage, ['year', 'week', 'percentage'])

df.coalesce(1).write.format('com.databricks.spark.csv').options(header='true').save('file:///mnt/scambiovbox/output/weatherdelay2')