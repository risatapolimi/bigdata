from pyspark import SparkContext
import pandas as pd
sc =SparkContext()

#Apertura file e divisione in campi
flights = sc.textFile("file:///mnt/scambiovbox/input") \
	.flatMap(lambda line: line.split("\n"))\
    .map(lambda line: line.split(",")) \
    .filter(lambda line: line[16]!="NA" and line[17]!="NA" and line[8]!="NA" and line[13]!="NA" and line[15]!="NA" and line[14]!="NA" and line[19]!="NA" and line[20]!="NA" and line[0] != "Year")\
    .map(lambda line: ((line[16]+"-"+line[17], line[8]), (int(line[13]), int(line[15])+int(line[14])+int(line[19])+int(line[20]))))

flights = flights.filter(lambda x: x[1][1]>=0) #filtra solo ritardi positivi
    #coppia ((depart-arrive,carrier), (airtime, totalExtraTime = depDelay+arrDelay+taxiIn+taxiOut))
    #coppia ((depart-arrive, carrier), (airtime, totalExtraTime))


    #coppia ((depart-arrive, carrier), airtime )
    #coppia ((depart-arrive, carrier), totalExtraTime )

airtimes = flights.map(lambda x: (x[0], x[1][0])).filter(lambda x: x[1]>0) #filtra airtime positivi e li mappa in apposita coppia
totalExtraTimes = flights.map(lambda x: (x[0], x[1][1]))

print "\nValori FLIGHT KEYS\n\n"
#print flights.keys().take(10)

	#media per ognuna delle tratte+compagnia del totalExtraTime

combineTotalExtraTime = totalExtraTimes.combineByKey(lambda value: (int(value), int(1)),
                             lambda x, value: (int(x[0]) + int(value), x[1] + 1),
                             lambda x, y: (int(x[0]) + int(y[0]), x[1] + y[1])) #coppia (chiave, (sommaExtraTime, countExtraTimes_voli))

mediaExtraTime = combineTotalExtraTime.map(lambda x: (x[0], 1.0*x[1][0]/x[1][1])) #coppia (chiave, media)

#per ogni tratta-compagnia, il piu veloce

airTimesFaster = airtimes.reduceByKey(min)


#print mediaExtraTime.take(10)

#print airTimesFaster.take(10)

joinAirAverage = airTimesFaster.join(mediaExtraTime) #coppia ((depart-arrive, carrier), (minairtime, mediatotalExtraTime))


#somma al minimo la media dei ritardi e riorganizza la coppia
mapperSum = joinAirAverage.map(lambda x: (x[0][0],(x[1][0]+x[1][1],x[0][1]))) #coppia ((depart-arrive ), (tempoMinimo, carrier))

#ordina
flightOrderByKey = mapperSum.sortBy(lambda x: x[1][0]).sortByKey()

#print flightOrderByKey.take(10)

#raggruppa per chiave
listInRoute = flightOrderByKey.groupByKey()

#estrae i primi tre più rapidi voli per ogni rotta
threeForRoute = listInRoute.map(lambda x: (x[0], (list(x[1])[0:3])))

#print threeForRoute.take(10)

from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

df = sqlContext.createDataFrame(threeForRoute, ['route', 'bestlist'])
pandasfriend = df.toPandas()

pandasfriend.to_csv("/mnt/scambiovbox/output/bestcompaniespos.csv", sep=';')
