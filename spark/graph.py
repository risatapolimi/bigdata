import pandas as pd
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource
from bokeh.palettes import Spectral3

output_file('/mnt/scambiovbox/charts/canceled.html')

df = pd.read_csv('/mnt/scambiovbox/output/canceled/part-00000-c851eade-1a52-4d56-a384-c6f8e06cf3f9-c000.csv')


#make sure date is a datetime format
df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
df.sort_values('date', ascending=True, inplace=True)
#print df
source = ColumnDataSource(df)

p = figure(x_axis_type='datetime', plot_width=800, plot_height=300)

p.line(x='date', y='percentage', line_width=2, source=source, legend='Canceled percentage')
#p = TimeSeries(source, index='date', legend=True, title="Canceled flights", ylabel='Calceled percentage')
p.yaxis.axis_label = 'Percentage of canceled flights per day'

show(p)