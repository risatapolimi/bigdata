from math import pi
import pandas as pd

from bokeh.io import show
from bokeh.models import LogColorMapper, BasicTicker, PrintfTickFormatter, ColorBar
from bokeh.plotting import figure, output_file

output_file('/mnt/scambiovbox/charts/penalty.html')
data = pd.read_csv('/mnt/scambiovbox/output/penalty1/part-00000-297dcbac-2aae-4446-8b94-9265e0fa2371-c000.csv')

data['year'] = data['year'].astype(str)


print data

data = data.sort_values(by=['year', 'week', 'airport'])
data = data.reset_index(drop=True)
data['period'] = data[['year', 'week']].astype(str).apply(lambda x: 'w'.join(x), axis=1)
data = data.set_index('period')

periods = set(data.index)
airports = set(data.airport)


colors = ["#75968f", "#a5bab7", "#c9d9d3", "#e2e2e2", "#dfccce", "#ddb7b1", "#cc7878", "#933b41", "#550b1d"]
mapper = LogColorMapper(palette=colors, low=data.score.min(), high=data.score.max())

TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom"

p = figure(title="Week penalty",
           x_range=list(periods), y_range=list(reverse(airports)),
           x_axis_location="above", plot_width=1200, plot_height=600,
           tools=TOOLS, toolbar_location='below',
           tooltips=[('week', '@year [@week]'),('airport', '@airport'), ('score', '@score') ])

p.grid.grid_line_color = None
p.axis.axis_line_color = None
p.axis.major_tick_line_color = None
p.axis.major_label_text_font_size = "5pt"
p.axis.major_label_standoff = 0
p.xaxis.major_label_orientation = pi / 3

p.rect(x="period", y="airport", width=1, height=1,
       source=data,
       fill_color={'field': 'score', 'transform': mapper},
       line_color=None)

color_bar = ColorBar(color_mapper=mapper, major_label_text_font_size="5pt",
                     ticker=BasicTicker(desired_num_ticks=len(colors)),
                     formatter=PrintfTickFormatter(format="%d"),
                     label_standoff=6, border_line_color=None, location=(0, 0))
p.add_layout(color_bar, 'right')

show(p)      # show the plot